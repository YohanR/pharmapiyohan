import React from 'react';
import logo from '../img/logo.svg';
import '../styles/Menu.css';




class Menu extends React.Component {

  render() {

    return (
      <>
        <nav className="navbar navbar-expand-lg navbar-light bg-light menu mb-3 ">
          <div className="container-fluid">
            <a className="navbar-brand"><img className="w-25" src={logo} alt="nom de ton image"></img></a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavDropdown">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <a><button className="bg-light border-0" onClick={() => this.props.pharma()} aria-current="page" ><span className='bleu ml-2 mr-2'>Liste des Pharmacies</span></button></a>
                </li>
                <li className="nav-item">
                  <button className="bg-light border-0" onClick={() => this.props.garde()} ><span className='bleu ml-2 mr-2' >Pharmacies de garde</span></button>
                </li>
                <li className="nav-item">
                  <button className="bg-light  border-0" onClick={() => this.props.formActive()}  ><span className='bleu ml-2 mr-2' >Ajout Pharmacie</span></button>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </>
    );
  }
}
export default Menu;
