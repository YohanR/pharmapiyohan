
import React from 'react';
import '../styles/Card.css'

class Card extends React.Component {

    render() {

        const { data, onDelete, onFormUpdate } = this.props


        if (this.props.color === 'red') {
            return (
                <div className='container-fluid w-90 card' >
                    <div className="row d-flex align-items-center">
                        <div className="col-8 col-lg-2 p-2">
                        </div>
                        <div className="col-8 col-lg-8 p-2">
                            <div className="row d-flex align-items-center">
                                <div className="col-12 col-lg-4 p-2">
                                    <p className='violet'><strong>{data.nom}</strong></p>
                                </div>
                                <div className="col-12 col-lg-3  p-2">
                                    <p className='violet'>De garde le : <strong>{data.garde}</strong></p>
                                </div>
                                <div className="col-12 col-lg-5   p-2">
                                    <p className='violet'>Adresse : <strong>{data.quartier}</strong> à <strong>{data.ville}</strong></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-8 col-lg-2 p-2">
                        </div>
                    </div>
                </div>)

        } else {

            return (
                <div className='container-fluid w-90' >
                    <div className="row d-flex align-items-center">
                        <div className="col-12 col-lg-8 p-2">
                            <div className="row d-flex align-items-center">
                                <div className="col-12 col-lg-4 p-2">
                                    <p className='violet'><strong>{data.nom}</strong></p>
                                </div>
                                <div className="col-12 col-lg-3  p-2">
                                    <p className='violet'>De garde le : <strong>{data.garde}</strong></p>
                                </div>
                                <div className="col-12 col-lg-5   p-2">
                                    <p className='violet'>Adresse : <strong>{data.quartier}</strong> à <strong>{data.ville}</strong></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-lg-4 p-2">
                            <button type="button" onClick={() => onFormUpdate(data)} className="btn btn-info bouton boutonModif">Modifier</button>
                            <button type="button" onClick={() => onDelete(data.id)} className="btn btn-danger bouton boutonSuppr">Supprimer</button>
                        </div>
                    </div>
                </div>
            )

        }




    }
}



export default Card;
