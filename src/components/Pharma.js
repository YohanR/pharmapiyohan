import React, { Component } from 'react';
import axios from 'axios';
import Card from './Card';
import FormUpdate from './FormUpdate';


class Pharma extends Component {
  state = {
    data: [],
    formUpdate: false
  }

  componentDidMount() {
    axios.get(`https://cherry-tart-07032.herokuapp.com/pharma`)
      .then(res => {
        const data = res.data;
        this.setState({ data });
      })
  }

  formUpdate(data) {
    this.setState({ data })
    this.setState({ formUpdate: true })
  }


  delete(id) {
    console.log(id);
    axios.delete(`https://cherry-tart-07032.herokuapp.com/pharma/${id}`)
      .then(res => {
        const data = this.state.data.filter(item => item.id !== id);
        this.setState({ data });
      })
  }

  render() {
    const { data } = this.state;

    if (this.state.formUpdate === true) {
      return <FormUpdate data={data} />
    } else {
      return (
        <>
          {
            data.length && data.map((data, index) => {
              return <Card key={index} onDelete={(id) => { this.delete(id) }} onFormUpdate={(data) => { this.formUpdate(data) }} data={data} />
            })
          }
        </>
      )
    }
  }
}

export default Pharma;