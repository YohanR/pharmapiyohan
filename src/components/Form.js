import React from 'react';
import axios from 'axios';
import '../styles/Form.css';


class Form extends React.Component {

    state = {
        RequestOk: false
    }

    handleChange = event => {
        const type = event.target.type;
        const name = event.target.name;
        if (type === 'text') {
            if (name === 'nom') {
                this.setState({ name: event.target.value });
            } else if (name === 'quartier') {
                this.setState({ quartier: event.target.value });
            } else if (name === 'ville') {
                this.setState({ city: event.target.value });
            }
        } else {
            this.setState({ garde: event.target.value });
        }
    }

    handleSubmit = (e) => {
        e.target[0].value = '';
        e.target[1].value = '';
        e.target[2].value = '';
        e.target[3].value = '';
        
        e.preventDefault();
        const data = {
            nom: this.state.name,
            quartier: this.state.quartier,
            ville: this.state.city,
            garde: this.state.garde
        }
        console.log(data);


        axios.post(`https://cherry-tart-07032.herokuapp.com/pharma`, data)
            .then(res => {
                this.setState({ RequestOk: true });
                console.log(res);
                console.log(res.data);
            })
    }

    render() {

        if (this.state.RequestOk === false) {
            return (
                <form className="formulaire" onSubmit={(e) => this.handleSubmit(e)} action="/">
                    <div className="col">
                        <label>Nom de la pharmacie
                        <input type="text" className="input-form form-control" name="nom" onChange={this.handleChange} placeholder="Nom de la pharmacie" aria-label="nom" required></input>
                        </label>
                    </div>
                    <div className="col">
                        <label>Quartier
                        <input type="text" className="input-form form-control" name="quartier" onChange={this.handleChange} placeholder="Quartier" aria-label="quartier" required></input>
                        </label>
                    </div>
                    <div className="col">
                        <label>Ville
                        <input type="text" className="input-form form-control" name="ville" onChange={this.handleChange} placeholder="Ville exemple Paris" aria-label="ville" required></input>
                        </label>
                    </div>
                    <div className="col">
                        <div className='container-fluid w-90 card border-0' >
                            <div className="row d-flex justify-content-center ">
                                <div className="col-8 col-lg-5 p-2">
                                </div>
                                <div className="col-12 col-lg-2 p-2">
                                    <select className="form-select input-form text-center" name="garde" onChange={this.handleChange} aria-label="Default select example" required>
                                        <option>Jour de garde</option>
                                        <option value="lundi">Lundi</option>
                                        <option value="mardi">Mardi</option>
                                        <option value="mercredi">Mercredi</option>
                                        <option value="jeudi">Jeudi</option>
                                        <option value="vendredi">Vendredi</option>
                                        <option value="samedi">Samedi</option>
                                        <option value="dimanche">Dimanche</option>
                                    </select>
                                </div>
                                <div className="col-8 col-lg-5 p-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-success bouton">Ajouter</button>
                </form>
            )
        } else {
            return (
                <form className="formulaire" onSubmit={(e) => this.handleSubmit(e)} action="/">
                    <p>Votre pharmacie a été ajouté</p>
                    <div className="col">
                        <label>Nom de la pharmacie
                    <input type="text" className="input-form form-control" name="nom" onChange={this.handleChange} placeholder="Nom de la pharmacie" aria-label="nom" required></input>
                        </label>
                    </div>
                    <div className="col">
                        <label>Quartier
                    <input type="text" className="input-form form-control" name="quartier" onChange={this.handleChange} placeholder="Quartier" aria-label="quartier" required></input>
                        </label>
                    </div>
                    <div className="col">
                        <label>Ville
                    <input type="text" className="input-form form-control" name="ville" onChange={this.handleChange} placeholder="Ville exemple Paris" aria-label="ville" required></input>
                        </label>
                    </div>
                    <div className="col">
                        <div className='container-fluid w-90 card border-0' >
                            <div className="row d-flex justify-content-center ">
                                <div className="col-8 col-lg-5 p-2">
                                </div>
                                <div className="col-12 col-lg-2 p-2">
                                    <select className="form-select input-form text-center" name="garde" onChange={this.handleChange} aria-label="Default select example" required>
                                        <option>Jour de garde</option>
                                        <option value="lundi">Lundi</option>
                                        <option value="mardi">Mardi</option>
                                        <option value="mercredi">Mercredi</option>
                                        <option value="jeudi">Jeudi</option>
                                        <option value="vendredi">Vendredi</option>
                                        <option value="samedi">Samedi</option>
                                        <option value="dimanche">Dimanche</option>
                                    </select>
                                </div>
                                <div className="col-8 col-lg-5 p-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-success bouton">Ajouter</button>
                </form>

            )
        }

    }
}


export default Form;