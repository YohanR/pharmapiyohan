import React from 'react';
import axios from 'axios';
import '../styles/Form.css';



class FormUpdate extends React.Component {

    state = {
        nom: '',
        quartier: '',
        ville: '',
        garde: '',
        RequestOk: false,
        Modif: 'Pharmacie modifiée',
        Modifstate: true
    }

    handleChange = event => {
        const type = event.target.type;
        const name = event.target.name;
        if (type === 'text') {
            if (name === 'nom') {
                this.setState({ nom: event.target.value });
            } else if (name === 'quartier') {
                this.setState({ quartier: event.target.value });
            } else if (name === 'ville') {
                this.setState({ ville: event.target.value });
            }
        } else {
            this.setState({ garde: event.target.value });
        }
    }

    componentDidMount() {
        this.setState(this.props.data)
    }
    handleSubmit = (e) => {
        e.preventDefault();
        axios.put(`https://cherry-tart-07032.herokuapp.com/pharma/${this.state.id}`, this.state)
            .then(res => {
                this.setState({
                    RequestOk: true,
                    Modifstate: true
                });
                console.log(res);
                console.log(res.data);
            })
    }

    ChangeStateModif() {
        if (this.state.Modifstate = false) {
            this.setState({ Modifstate: true })
        }
    }

    render() {
        const data = this.state;
        console.log(this.state.Modifstate);
        if (this.state.RequestOk === false) {
            return (
                <form className="formulaire" onSubmit={(e) => this.handleSubmit(e)} action="/">
                    <div className="col">
                        <label>Nom
                    <input type="text" name="nom" className="form-control" onChange={this.handleChange} value={data.nom} aria-label="nom" required></input>
                        </label>
                    </div>
                    <div className="col">
                        <label>Quartier
                <input type="text" name="quartier" className="form-control" onChange={this.handleChange} value={data.quartier} aria-label="quartier" required></input>
                        </label>
                    </div>
                    <div class='row d-flex'>
                        <div className="col-2 col-lg-5 p-2">
                        </div>
                        <div className="col-8 col-lg-2">
                            <label>Ville
                        <input type="text" name="ville" className="form-control" onChange={this.handleChange} value={data.ville} aria-label="ville" required></input>
                            </label>
                        </div>
                        <div className="col-2 col-lg-5 p-2">
                        </div>
                    </div>
                    <div className="col">
                        <label>Jour de garde
                <select className="form-select liste" name="garde" onChange={this.handleChange} value={data.garde} aria-label="Default select example" required>
                                <option>Jour de garde</option>
                                <option value="lundi">Lundi</option>
                                <option value="mardi">Mardi</option>
                                <option value="mercredi">Mercredi</option>
                                <option value="jeudi">Jeudi</option>
                                <option value="vendredi">Vendredi</option>
                                <option value="samedi">Samedi</option>
                                <option value="dimanche">Dimanche</option>
                            </select>
                        </label>
                    </div>
                    <button type="submit" onClick={() => this.ChangeStateModif()} className="btn btn-success bouton">Modifier</button>
                </form>
            )
        } else {
            return (
                <div>
                    {this.state.Modif}
                    <form className="formulaire" onSubmit={(e) => this.handleSubmit(e)} action="/">
                        <div className="col">
                            <label>Nom
                    <input type="text" name="nom" className="form-control" onChange={this.handleChange} value={data.nom} aria-label="nom" required></input>
                            </label>
                        </div>
                        <div className="col">
                            <label>Quartier
                <input type="text" name="quartier" className="form-control" onChange={this.handleChange} value={data.quartier} aria-label="quartier" required></input>
                            </label>
                        </div>
                        <div class='row d-flex'>
                            <div className="col-2 col-lg-5 p-2">
                            </div>
                            <div className="col-8 col-lg-2">
                                <label>Ville
                        <input type="text" name="ville" className="form-control" onChange={this.handleChange} value={data.ville} aria-label="ville" required></input>
                                </label>
                            </div>
                            <div className="col-2 col-lg-5 p-2">
                            </div>
                        </div>
                        <div className="col">
                            <label>Jour de garde
                <select className="form-select liste" name="garde" onChange={this.handleChange} value={data.garde} aria-label="Default select example" required>
                                    <option>Jour de garde</option>
                                    <option value="lundi">Lundi</option>
                                    <option value="mardi">Mardi</option>
                                    <option value="mercredi">Mercredi</option>
                                    <option value="jeudi">Jeudi</option>
                                    <option value="vendredi">Vendredi</option>
                                    <option value="samedi">Samedi</option>
                                    <option value="dimanche">Dimanche</option>
                                </select>
                            </label>
                        </div>
                        <button type="submit" onClick={() => this.ChangeStateModif()} className="btn btn-success bouton">Modifier</button>
                    </form>
                </div>
            )

        }
    }
}
export default FormUpdate;