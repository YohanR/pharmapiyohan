import React, { Component } from 'react';
import axios from 'axios';
import Card from './Card';

class Garde extends Component {
  state = {
    data: [],
  }

  componentDidMount() {
    axios.get(`https://cherry-tart-07032.herokuapp.com/pharma-garde`)
      .then(res => {
        const data = res.data;
        this.setState({ data });
      })
  }

  render() {
    const { data } = this.state;
    return (

      <>
        {
          data.length && data.map((data, index) => {
            return <Card key={index} data={data} color="red" />
          })
        }
      </>
    )
  }
}




export default Garde;