import React from 'react';
import Menu from './components/Menu';
import 'bootstrap/dist/css/bootstrap.min.css';
import Pharma from './components/Pharma';
import Form from './components/Form';
import Garde from './components/Garde';
import './App.css';


class App extends React.Component {

  state = {
    form: false,
    garde: false,
    pharma:true
  }

  formActive() {
    this.setState({ form: true });
    this.setState({ pharma: false });
    this.setState({ garde: true })
  }
  garde() {
    this.setState({ garde: true })
    this.setState({ pharma: false });
    this.setState({ form: false });
  }

  pharma(){
    this.setState({ garde: false })
    this.setState({ pharma: true });
    this.setState({ form: false });
  }

  render() {
    const { form, garde } = this.state;
    return (

      <div className="App">
        <Menu formActive={() => this.formActive(this)}  pharma={()=>this.pharma(this)} garde={() => this.garde(this)} />
        {form ? <Form /> : garde ? <Garde /> : <Pharma />}
      </div>)

  }
}

export default App;
