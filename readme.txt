maquettes : https://www.figma.com/file/5DODLjyJVDvlIql1UOGCEy/Untitled?node-id=0%3A1
Site : https://pharmapiyohan.herokuapp.com/


Contexte du projet

Suite à votre excellente prestation de Pharmapi en Symfony. Vos etes chargé de développer l'application web permettant de :

    consulter la liste des pharmacies
    consulter la liste des pharmacies de garde
    ajouter, éditer, supprimer des pharmacies
    le site doit être une PWA

Créer les maquettes les plus attrayantes possibles

Utiliser Axios pour les requêtes à l'api.

Palette de couleurs a utiliser:

#00FF00, #007FFF, #FF00FF, #FF8000

Bonus: Développer la version mobile avec React-Native

​

​
Modalités pédagogiques

Travail individuel
Critères de performance

    Les maquettes et le rendu html respectent la palette couleur proposée
    Utilisation de Axios pour les requêtes
    Plusieurs composants ont été utilisés(ex: Navbar, Footer, Formulaire d'ajout/édition, ...)
    Pas de plugin React (sauf axios)

Modalités d'évaluation

Revue de code avec le formateur
Livrables

Dépôt gitlab, site en ligne
